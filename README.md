![DWMC-16 The Emulator](DWMC-16_The_Emulator.png)

This Project is to create a low level emulator for the DWMC-16, to test
out ideas and code.

It is written in Python and uses TK for its GUI.

The emulator goes down to a module level, simulating every single module
used in the construction of the DWMC-16
