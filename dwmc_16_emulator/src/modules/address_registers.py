'''Emulation of the Basic Address Registers for the DWMC-16'''

from dwmc_16_emulator.src.modules.constants import CadrId
from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import ModuleFunctionInterface, BusData
from dwmc_16_emulator.src.modules.registers import Register


class AddressRegister(ModuleFunctionInterface):
    '''Basic Address Register for the DWMC-16, like the Z-Register'''

    def __init__(self, high_register: Register, low_register: Register, control_line: str = "ZAE") -> None:
        '''Initialization of the address register'''
        if not isinstance(low_register, Register) or not isinstance(high_register, Register):
            raise ValueError("No High or Low Register was referred to the initialization")
        if not isinstance(control_line, str):
            raise ValueError("No Control Line string was referred to the initialization")
        self.high_register = high_register
        self.low_register = low_register
        self.control_line = control_line

    def execute_function(self, bus: BusData) -> BusData:
        '''Writes the contents of the high and low address register to the address bus'''
        if not bus.control_signals[self.control_line]:
            raise ControlBusError("Unable to write Address without set control line")
        return self._write_to_address_bus(bus)

    def _write_to_address_bus(self, bus: BusData) -> BusData:
        data = bus.data
        control_signals = bus.control_signals
        address_high = self.high_register.value
        address_low = self.low_register.value
        address = address_low | (address_high << 16)
        return BusData(data=data, address=address, control_signals=control_signals)


class ModifiableAddressRegister(AddressRegister):
    '''Modifiable Address Register with increment and decrement functions for
    Stack Pointer and Program Counter'''

    def __init__(
        self,
        high_register: Register,
        low_register: Register,
        control_line: str = "ZAE",
        increment_line: str = "ISP",
        decrement_line: str = "DSP",
    ) -> None:
        if not isinstance(increment_line, str) or not isinstance(decrement_line, str):
            raise ValueError("No Increment or Decrement Line string was referred to the initialization")
        self.increment_line = increment_line
        self.decrement_line = decrement_line
        super().__init__(high_register, low_register, control_line)

    def execute_function(self, bus: BusData) -> BusData:
        '''Writes the contents of the high and low address register to the address bus
        Increments or Decrements the Address in the registers
        '''
        if (
            not bus.control_signals[self.control_line]
            and not bus.control_signals[self.increment_line]
            and not bus.control_signals[self.decrement_line]
        ):
            raise ControlBusError("Unable to write/increment/decrement Address without set control line")
        if (
            not bus.control_signals[self.control_line]
            and bus.control_signals[self.increment_line]
            and bus.control_signals[self.decrement_line]
        ):
            raise ControlBusError("Unable to increment and decrement the Address at the same time")
        if bus.control_signals[self.control_line] and (
            bus.control_signals[self.increment_line] or bus.control_signals[self.decrement_line]
        ):
            raise ControlBusError("Unable to write Address while icrementing/decrementing")
        if bus.control_signals[self.control_line]:
            bus = self._write_to_address_bus(bus)
        if bus.control_signals[self.increment_line]:
            self._increment()
        if bus.control_signals[self.decrement_line]:
            self._decrement()
        return bus

    def _increment(self):
        '''Increments the address inside Address register over high and low registers'''
        high_word = self.high_register.value
        low_word = self.low_register.value
        address = (low_word | (high_word << 16)) + 1
        if address > 0xFFFFFF:
            address = 0x000000
        low_word = address & 0xFFFF
        high_word = (address & 0xFF0000) >> 16
        self.high_register.value = high_word
        self.low_register.value = low_word

    def _decrement(self):
        '''Increments the address inside Address register over high and low registers'''
        high_word = self.high_register.value
        low_word = self.low_register.value
        address = (low_word | (high_word << 16)) - 1
        if address < 0x000000:
            address = 0xFFFFFF
        low_word = address & 0xFFFF
        high_word = (address & 0xFF0000) >> 16
        self.high_register.value = high_word
        self.low_register.value = low_word
