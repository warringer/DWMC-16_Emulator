'''Emulatiopn of the DWMC-16 ALU'''

from dwmc_16_emulator.src.modules.constants import CadrId, AluOp
from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import ModuleFunctionInterface, BusData
from dwmc_16_emulator.src.modules.register_bank import RegisterBank
from dwmc_16_emulator.src.modules.registers import Register


class Alu(ModuleFunctionInterface):
    '''ALU for the DWMC-16'''

    def __init__(self, register_bank: RegisterBank) -> None:
        self.input_register_a = Register()
        self.input_register_b = Register()
        self.output_register = Register()
        self.flag_register = register_bank.registers[CadrId.F]
        self.register_bank = register_bank
        super().__init__()

    def execute_function(self, bus: BusData) -> BusData:
        if not bus.control_signals["ALUS"]:
            raise ControlBusError("ALU not Selected for operation")
        if int(bus.control_signals["AOPS"]) > 0x1F:
            raise ControlBusError("ALU Operation does not exist")
        if int(bus.control_signals["AREGA"]) > 0xF or int(bus.control_signals["AREGB"]) > 0xF:
            raise ControlBusError("Selected Register does not exist")

        register_a = bus.control_signals["AREGA"]
        register_b = bus.control_signals["AREGB"]
        alu_op = bus.control_signals["AOPS"]
        self.input_register_a = self.register_bank.registers[register_a]
        self.input_register_b = self.register_bank.registers[register_b]
        self.output_register = self.register_bank.registers[register_a]
        input_a = self.input_register_a.value
        if bus.control_signals["ARDB"]:
            input_b = bus.data
        else:
            input_b = self.input_register_b.value

        result = None
        match alu_op:
            case AluOp.ADD:
                result = self._add(input_a, input_b)
            case AluOp.INC:
                input_b = self._bit_generator(CadrId.B00)
                result = self._add(input_a, input_b)
            case AluOp.SUB:
                input_b = ~int(input_b)
                result = self._add(input_a, input_b, carry=1)
            case AluOp.DEC:
                input_b = self._bit_generator(CadrId.B00)
                input_b = ~input_b
                result = self._add(input_a, input_b, carry=1)
            case AluOp.AND:
                result = self._and(input_a, input_b)
            case AluOp.OR:
                result = self._or(input_a, input_b)
            case AluOp.XOR:
                result = self._xor(input_a, input_b)
            case AluOp.NOT:
                result = self._not(input_a)
            case AluOp.LSR:
                result, shift_carry = self._rotate_right(input_a)
                if (self.flag_register.value and self._bit_generator(CadrId.CA)) > 0:  # Carry Flag == 1
                    result |= 0x8000
                if shift_carry:
                    self.flag_register.set_flag(CadrId.CA)
                else:
                    self.flag_register.reset_flag(CadrId.CA)
            case AluOp.LSL:
                result, shift_carry = self._rotate_left(input_a)
                if (self.flag_register.value and self._bit_generator(CadrId.CA)) > 0:  # Carry Flag == 1
                    result |= 0x0001
                if shift_carry > 0:
                    self.flag_register.set_flag(CadrId.CA)
                else:
                    self.flag_register.reset_flag(CadrId.CA)
            case AluOp.LRR:
                result, shift_carry = self._rotate_right(input_a)
                if shift_carry > 0:  # Carry Flag == 1
                    result |= 0x8000
            case AluOp.LRL:
                result, shift_carry = self._rotate_left(input_a)
                if shift_carry > 0:  # Carry Flag == 1
                    result |= 0x0001
            case AluOp.LSB:
                result = self._swap_bytes(input_a)
            case AluOp.SB:
                result = self._set_bit(input_a, bus.control_signals["CADR"])
            case AluOp.RB:
                result = self._reset_bit(input_a, bus.control_signals["CADR"])
            case AluOp.TB:
                self._test_bit(input_a, bus.control_signals["CADR"])
            case AluOp.TZ:
                self._test_zero(input_a)
            case AluOp.TEQ:
                self._test_equal(input_a, input_b)
            case AluOp.TGT:
                self._test_greater_then(input_a, input_b)
            case AluOp.TGE:
                self._test_equal(input_a, input_b)
                self._test_greater_then(input_a, input_b)
            case _:
                raise ControlBusError("Unknown Operation")

        if result is not None and bus.control_signals["AWOE"]:
            self.output_register.value = result
        return bus

    def _add(self, input_a: int, input_b: int, carry: int = 0) -> int:
        if (self.flag_register.value and self._bit_generator(CadrId.CA)) > 0:
            carry = 1
        result = input_a + input_b + carry
        self._flag_carry(result)
        self._flag_zero(result)
        self._flag_negative(result)
        self._flag_overflow(result)
        result &= 0xFFFF
        return result

    def _not(self, input_a: int) -> int:
        result = ~input_a
        result &= 0xFFFF
        self._flag_zero(result)
        return result

    def _and(self, input_a: int, input_b: int) -> int:
        result = input_a & input_b
        self._flag_zero(result)
        return result

    def _or(self, input_a: int, input_b: int) -> int:
        result = input_a | input_b
        self._flag_zero(result)
        return result

    def _xor(self, input_a: int, input_b: int) -> int:
        result = input_a ^ input_b
        self._flag_zero(result)
        return result

    def _rotate_right(self, input_a: int) -> tuple:
        carry = input_a & 0x0001
        result = input_a >> 1
        self._flag_zero(result)
        return result, carry

    def _rotate_left(self, input_a: int) -> tuple:
        carry = input_a & 0x8000
        result = input_a << 1
        result = result & 0xFFFF
        self._flag_zero(result)
        return result, carry

    def _swap_bytes(self, input_a: int) -> int:
        high_byte = input_a & 0xFF00
        high_byte = high_byte >> 8
        low_byte = input_a & 0x00FF
        result = high_byte + (low_byte << 8)
        return result

    def _set_bit(self, input_a: int, bit: CadrId) -> int:
        bit_mask = self._bit_generator(bit)
        result = input_a | bit_mask
        return result

    def _reset_bit(self, input_a: int, bit: CadrId) -> int:
        bit_mask = self._bit_generator(bit)
        bit_mask = ~bit_mask
        bit_mask &= 0xFFFF
        result = input_a & bit_mask
        return result

    def _test_bit(self, input_a: int, bit: CadrId) -> None:
        bit_mask = self._bit_generator(bit)
        result = input_a & bit_mask
        self._flag_zero(result)

    def _test_zero(self, input_a: int) -> None:
        self._flag_zero(input_a)

    def _test_equal(self, input_a: int, input_b: int) -> None:
        if input_a == input_b:
            self.flag_register.set_flag(CadrId.EQ)

    def _test_greater_then(self, input_a: int, input_b: int) -> None:
        result = input_a - input_b
        if result < 0:
            self.flag_register.set_flag(CadrId.NG)

    def _flag_zero(self, result: int) -> None:
        if result == 0:
            self.flag_register.set_flag(CadrId.ZE)

    def _flag_carry(self, result: int) -> None:
        if result > 0xFFFF:
            self.flag_register.set_flag(CadrId.CA)

    def _flag_negative(self, result: int) -> None:
        result &= 0xFFFF
        if 0xFFFF >= result > 0x7FFF:
            self.flag_register.set_flag(CadrId.NG)

    def _flag_equal(self) -> None:
        if self.input_register_a == self.input_register_b:
            self.flag_register.set_flag(CadrId.EQ)

    def _flag_overflow(self, result) -> None:
        pass

    def _bit_generator(self, bit: CadrId) -> int:
        result = 1 << int(bit)
        return result
