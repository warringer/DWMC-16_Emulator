'''Emulation of the Bus system of the DWMC-16
'''


from dwmc_16_emulator.src.modules.interfaces import ModuleFunctionInterface


class Bus:
    '''Bus system of the DWMC-16'''

    def __init__(self) -> None:
        self.address_bus: int = 0x000000
        self.data_bus: int = 0x0000
        self.control_bus: dict = {
            "ALUS": False,  # ALU Select
            "AWOE": False,  # ALU Write Output Enable (Write ALU to Register A)
            "ARDB": False,  # ALU Read Data Bus (Write Databus to Input B)
            "AREGA": 0x0,  # ALU Input Register A/Output Register (4 bit)
            "AREGB": 0x0,  # ALU Input Register B (4 bit)
            "AOPS": 0x0,  # ALU Operation Select (5 bit)
            "RWE": False,  # Register Write Enagle
            "RRE": False,  # Register Read Enable
            "CLK": False,  # Clock
            "CADR": 0x0,  # Control Address for Register
            # Flag/Test Bit/Interrupt Vector Select (4 bit)
            "FS": False,  # Flag Select
            "BT": False,  # Bit Test
            "FT": False,  # Flag Test
            "BFS": False,  # Bit/Flag Set
            "PCAE": False,  # Program Counter Adress Enable
            "SPAE": False,  # Stack Pointer Adress Enable
            "ZIAE": False,  # Z Index Adress Enable
            "ISP": False,  # Increment Stack Pointer Signal
            "DSP": False,  # Decrement Stack Pointer Signal
            "IPC": False,  # Increment Program Counter Signal
            "INTAE": False,  # Interrupt Vector Adress Enable
            "INT1": False,  # Interrupt 1 Signal (External)
            "INT2": False,  # Interrupt 2 Signal (External)
            "INT3": False,  # Interrupt 3 Signal (External)
            "INT4": False,  # Interrupt 4 Signal (External)
            "INT5": False,  # Interrupt 5 Signal (External)
            "INT6": False,  # Interrupt 6 Signal (External)
            "INT7": False,  # Interrupt 7 Signal
            "IO": False,  # IO Select Line (External)
            "WE": False,  # Write Enable (External)
            "RE": False,  # Read Enable (External)
        }
        self.modules: dict[str, ModuleFunctionInterface] = {}
