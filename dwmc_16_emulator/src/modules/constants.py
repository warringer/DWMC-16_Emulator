'''Constants used in the DWMC-16 emulator'''

from enum import Enum


class CadrId(str, Enum):
    '''Class containing Constants for the CADR Lines of the Control Bus'''

    # Registers
    R00 = 0x0
    R01 = 0x1
    R02 = 0x2
    R03 = 0x3
    R04 = 0x4
    R05 = 0x5
    R06 = 0x6
    R07 = 0x7
    R08 = 0x8
    R09 = 0x9
    R10 = 0xA
    R11 = 0xB
    R12 = 0xC
    R13 = 0xD
    R14 = 0xE
    R15 = 0xF
    BT = 0x8
    F = 0x9  # Flag Register
    PCL = 0xA  # Program Counter Low Register
    PCH = 0xB  # Program Counter High Register
    SPL = 0xC  # Stack Pointer Low Register
    SPH = 0xD  # Stack Pointer High Register
    WL = 0x2  # W Low Register
    WH = 0x3  # W High Register
    XL = 0x4  # X Low Register
    XH = 0x5  # X High Register
    YL = 0x6  # Y Low Register
    YH = 0x7  # Y High Register
    ZL = 0xE  # Z Index Low Register
    ZH = 0xF  # Z Index High Register

    # Flags
    ZE = 0xF  # Zero Flag
    CA = 0xD  # Carry Flag
    QC = 0xC  # Quarter Carry Flag
    HC = 0xB  # Half Carry Flag
    TC = 0xA  # Three Quaters Carry Flag
    EQ = 0x8  # Equality Flag
    NG = 0x7  # Negative Flag
    OF = 0x6  # Overflow Flag
    IO = 0x5  # IO Memory Access Flag
    IE = 0x3  # Interrupt Enable Flag
    I0 = 0x2  # Interrupt 0 Flag
    I1 = 0x2  # Interrupt 1 Flag
    I3 = 0x1  # Interrupt 2 Flag

    # Bit Test
    B00 = 0x0
    B01 = 0x1
    B02 = 0x2
    B03 = 0x3
    B04 = 0x4
    B05 = 0x5
    B06 = 0x6
    B07 = 0x7
    B08 = 0x8
    B09 = 0x9
    B10 = 0xA
    B11 = 0xB
    B12 = 0xC
    B13 = 0xD
    B14 = 0xE
    B15 = 0xF

    # Interrupt Vectors
    RV = 0x0  # Reset Vector
    INT1 = 0x1  # Interrupt Vector 1
    INT2 = 0x2  # Interrupt Vector 2
    INT3 = 0x3  # Interrupt Vector 3
    INT4 = 0x4  # Interrupt Vector 4
    INT5 = 0x5  # Interrupt Vector 5
    INT6 = 0x6  # Interrupt Vector 6
    INT7 = 0x7  # Interrupt Vector 7


class AluOp(str, Enum):
    '''Class Containing the ALU operations'''

    ADD = 0x00  # Add
    INC = 0x01  # Increment
    SUB = 0x02  # Substract
    DEC = 0x03  # Decrement
    AND = 0x04  # Bitwise AND
    OR = 0x05  # Bitwise OR
    XOR = 0x06  # Bitwise XOR
    NOT = 0x07  # Bitwise NOT
    LSR = 0x08  # Logic Shift Right
    LSL = 0x09  # Logic Shift Left
    LRR = 0x0A  # Logic Rotate Right
    LRL = 0x0B  # Logic Rotate Left
    LSB = 0x0C  # Logic Byte Swap
    SB = 0x0D  # Set Bit
    RB = 0x0E  # Reset Bit
    TB = 0x10  # Test Bit
    TZ = 0x11  # Test if Input A is zero
    TEQ = 0x12  # Test if Inputs are equal
    TGT = 0x13  # Test if A is greater then B
    TGE = 0x14  # Test if aA is Greater then or equal to B
