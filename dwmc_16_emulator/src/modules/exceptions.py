'''Custom Exceptions for the DWMC-16 emulator
'''


class ControlBusError(Exception):
    '''Raised when a Control Bus Error occurs'''
