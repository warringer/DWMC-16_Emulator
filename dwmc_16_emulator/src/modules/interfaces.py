'''Implements abstract interfaces for the DWMC-16 emulator'''

from abc import ABC, abstractmethod
from collections import namedtuple

BusData = namedtuple('BusData', 'data address control_signals')


class ModuleFunctionInterface(ABC):
    @abstractmethod
    def execute_function(self, bus: BusData) -> BusData:
        '''Implements the function of the module'''
