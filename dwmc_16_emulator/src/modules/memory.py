'''Emulation of the DWMC-16 memory.'''

from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import ModuleFunctionInterface, BusData


class Memory(ModuleFunctionInterface):
    '''Memory bank for DWMC-16 systems.'''

    def __init__(self, size: int = 0xFFFFFF, rom_size: int = 0x020000) -> None:
        '''Initialize the memory.

        :param size: The size of the memory
        :return: None
        '''
        if (size < 0x000000) or (size > 0xFFFFFF):
            raise ValueError("Memory size is not valid")
        self.size: int = size
        if (rom_size < 0x000000) or (rom_size > self.size):
            raise ValueError("ROM size out of bounds")
        self.rom_size = rom_size
        self.memory: list[int] = [0] * self.size
        print(len(self.memory))

    def __getitem__(self, address: int) -> int:
        """Get the value at the specified address.

        :param address: The address to read from
        :return: The value at the specified address
        """
        if (address < 0x000000) or (address > 0xFFFFFF):
            raise ValueError("Memory address is not valid")
        return self.memory[address]

    def __setitem__(self, address: int, value: int) -> int:
        """Set the value at the specified address.

        :param address: The address to write to
        :param value: The value to write to the address
        :return: None
        """
        if (address < 0x000000) or (address > 0xFFFFFF):
            raise ValueError("Memory address is not valid")
        if value.bit_length() > 16:
            raise ValueError("Value too large")
        self.memory[address] = value
        return self.memory[address]

    def execute_function(self, bus) -> BusData:
        control_signals = bus.control_signals
        data = bus.data
        address = bus.address
        if control_signals["IO"]:
            raise ControlBusError("Unable to access memory if IO Flag is set")
        if not control_signals["WE"] and not control_signals["RE"]:
            raise ControlBusError("Unable to access memory if Write and Read Enable is not set")
        if control_signals["RE"] and control_signals["WE"]:
            raise ControlBusError("Unable to access memory when both Read and Write Enable are set")
        if (address < 0x000000) or (address > 0xFFFFFF):
            raise ValueError("Memory address is not valid")
        if control_signals["WE"]:
            if address > self.rom_size:
                self.memory[address] = data
        if control_signals["RE"]:
            data = self.memory[address]
        return BusData(data, address, control_signals)
