'''Emulation of the Register bank of the DWMC-16'''

from dwmc_16_emulator.src.modules.address_registers import AddressRegister, ModifiableAddressRegister
from dwmc_16_emulator.src.modules.constants import CadrId
from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import ModuleFunctionInterface, BusData
from dwmc_16_emulator.src.modules.registers import DoubleRegister, FlagRegister, Register


class RegisterBank(ModuleFunctionInterface):
    '''Register Bank of the DWMC-16'''

    def __init__(self) -> None:
        self.registers = {
            CadrId.R00: Register(),
            CadrId.R01: Register(),
            CadrId.R02: Register(),
            CadrId.R03: Register(),
            CadrId.R04: Register(),
            CadrId.R05: Register(),
            CadrId.R06: Register(),
            CadrId.R07: Register(),
            CadrId.BT: Register(),
            CadrId.F: FlagRegister(),
            CadrId.PCL: Register(),
            CadrId.PCH: Register(),
            CadrId.SPL: Register(),
            CadrId.SPH: Register(),
            CadrId.ZL: Register(),
            CadrId.ZH: Register(),
        }
        self.address_registers = {
            "SP": ModifiableAddressRegister(
                low_register=self.registers[CadrId.SPL],
                high_register=self.registers[CadrId.SPH],
                control_line="SPAE",
                increment_line="ISP",
                decrement_line="DSP",
            ),
            "PC": ModifiableAddressRegister(
                low_register=self.registers[CadrId.PCL],
                high_register=self.registers[CadrId.PCH],
                control_line="PCAE",
                increment_line="IPC",
            ),
            "ZI": AddressRegister(
                low_register=self.registers[CadrId.ZL], high_register=self.registers[CadrId.ZH], control_line="ZAE"
            ),
        }
        self.double_registers = {
            "W": DoubleRegister(low_register=self.registers[CadrId.R02], high_register=self.registers[CadrId.R03]),
            "X": DoubleRegister(low_register=self.registers[CadrId.R04], high_register=self.registers[CadrId.R04]),
            "Y": DoubleRegister(low_register=self.registers[CadrId.R06], high_register=self.registers[CadrId.R07]),
            "PC": DoubleRegister(low_register=self.registers[CadrId.R10], high_register=self.registers[CadrId.R11]),
            "SP": DoubleRegister(low_register=self.registers[CadrId.R12], high_register=self.registers[CadrId.R13]),
            "Z": DoubleRegister(low_register=self.registers[CadrId.R14], high_register=self.registers[CadrId.R15]),
        }

    def execute_function(self, bus: BusData) -> BusData:
        '''Executes the operation on the selected Register'''
        control_lines = bus.control_signals
        register = control_lines["CADR"]
        if control_lines["RWE"] or control_lines["RRE"]:
            return self.registers[register].execute_function(bus)
        if control_lines["ZAE"]:
            return self.address_registers["ZI"].execute_function(bus)
        if control_lines["SPAE"] or control_lines["ISP"] or control_lines["DSP"]:
            return self.address_registers["SP"].execute_function(bus)
        if control_lines["PCAE"] or control_lines["IPC"]:
            return self.address_registers["PC"].execute_function(bus)
        raise ControlBusError("Unable to access any Registers")
