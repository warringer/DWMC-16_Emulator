'''Emulation of a Basic Register Types of the DWMC-16'''

from collections import namedtuple
from dwmc_16_emulator.src.modules.constants import CadrId
from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import ModuleFunctionInterface, BusData


class Register(ModuleFunctionInterface):
    '''Basic Register for the DWMC-16'''

    def __init__(self, initial_value=0x0000) -> None:
        if initial_value.bit_length() > 16:
            raise ValueError("Value out of bounds")
        if initial_value < 0x0000:
            raise ValueError("Value out of bounds")
        self.value = initial_value

    def execute_function(self, bus: BusData) -> BusData:
        if not bus.control_signals["RWE"] and not bus.control_signals["RRE"]:
            raise ControlBusError("Unable to access register if Register Read and Write Enable is not set")
        if bus.control_signals["RRE"] and bus.control_signals["RWE"]:
            raise ControlBusError("Unable to access register if Register Read and Write Enable are set")
        bus = self._handle_data(bus)
        return bus

    def _handle_data(self, bus):
        '''Handles data write and reads'''
        data = bus.data
        if data.bit_length() > 16:
            raise ValueError("Value too large")
        if bus.control_signals["RWE"]:
            self.value = data
        if bus.control_signals["RRE"]:
            data = self.value
        return BusData(data=data, address=bus.address, control_signals=bus.control_signals)


class FlagRegister(Register):
    '''Flag Register'''

    def execute_function(self, bus: BusData) -> BusData:
        if not bus.control_signals["RWE"] and not bus.control_signals["RRE"] and not bus.control_signals["FT"]:
            raise ControlBusError(
                "Unable to access register if Register Read and Write Enable and Bit Test are not set"
            )
        if bus.control_signals["RRE"] and bus.control_signals["RWE"] and bus.control_signals["FT"]:
            raise ControlBusError("Unable to access register if Register Read and Write Enable and Bit Test are set")
        bus = self._handle_data(bus)
        if bus.control_signals["FT"]:
            bfs = False
            if self.value & (1 << int(bus.control_signals["CADR"])):
                bfs = True
            bus.control_signals["BFS"] = bfs
        return BusData(data=bus.data, address=bus.address, control_signals=bus.control_signals)

    def set_flag(self, flag: CadrId) -> None:
        '''Function to set a flag inside the Flag register'''
        self.value = self.value | (1 << int(flag))

    def reset_flag(self, flag: CadrId) -> None:
        '''Function to reset a flag inside the Flag register'''
        self.value = self.value & ~(1 << int(flag))


DoubleRegister = namedtuple('DoubleRegister', ['low_register', 'high_register'])
