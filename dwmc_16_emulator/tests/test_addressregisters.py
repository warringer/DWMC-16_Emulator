'''Unit Test for Address Register Modules'''

import pytest
from dwmc_16_emulator.src.modules.address_registers import AddressRegister, ModifiableAddressRegister
from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import BusData
from dwmc_16_emulator.src.modules.registers import Register


@pytest.fixture()
def address_high_word():
    return 0x000F


@pytest.fixture()
def address_low_word():
    return 0x0F0F


@pytest.fixture()
def high_register():
    return Register()


@pytest.fixture()
def low_register():
    return Register()


@pytest.fixture()
def sp_increment():
    return "ISP"


@pytest.fixture()
def sp_decrement():
    return "DSP"


def test_address_register_init(high_register, low_register):
    '''Test if the initialization works as expected'''
    low_reg = low_register
    high_reg = high_register
    reg = AddressRegister(high_register=high_reg, low_register=low_reg, control_line="SPAE")
    assert reg.high_register == high_reg
    assert reg.low_register == low_reg
    assert reg.control_line == "SPAE"


def test_address_register_init_exceptions(high_register, low_register):
    '''Test if the initialization throws the expected esceptions'''
    low_reg = low_register
    high_reg = high_register
    with pytest.raises(ValueError) as excinfo:
        _ = AddressRegister(high_register=None, low_register=low_reg)
    assert str(excinfo.value) == "No High or Low Register was referred to the initialization"
    with pytest.raises(ValueError) as excinfo:
        _ = AddressRegister(high_register=high_reg, low_register=None)
    assert str(excinfo.value) == "No High or Low Register was referred to the initialization"
    with pytest.raises(ValueError) as excinfo:
        _ = AddressRegister(high_register=high_reg, low_register=low_reg, control_line=None)
    assert str(excinfo.value) == "No Control Line string was referred to the initialization"


def test_address_register_execute_function(high_register, low_register, address_high_word, address_low_word):
    '''Test is execute function works as expected'''
    data = 0
    low_reg = low_register
    high_reg = high_register
    low_reg.value = address_low_word
    high_reg.value = address_high_word
    control_signal = {"ZAE": True}
    address = 0x000000
    bus = BusData(data=data, address=address, control_signals=control_signal)
    expected_address = address_low_word | (address_high_word << 16)
    reg = AddressRegister(high_register=high_reg, low_register=low_reg)
    return_bus = reg.execute_function(bus)
    assert return_bus.address == expected_address


def test_address_register_execute_function_exceptions(high_register, low_register, address_high_word, address_low_word):
    '''Test is execute function works as expected'''
    data = 0
    low_reg = low_register
    high_reg = high_register
    low_reg.value = address_low_word
    high_reg.value = address_high_word
    control_signal = {"ZAE": False}
    address = 0x000000
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = AddressRegister(high_register=high_reg, low_register=low_reg)
    with pytest.raises(ControlBusError) as excinfo:
        return_bus = reg.execute_function(bus)
    assert str(excinfo.value) == "Unable to write Address without set control line"


def test_modifyable_address_register_init(high_register, low_register, sp_increment, sp_decrement):
    '''Test if the initialization works as expected'''
    low_reg = low_register
    high_reg = high_register
    reg = ModifiableAddressRegister(
        high_register=high_reg,
        low_register=low_reg,
        control_line="SPAE",
        increment_line=sp_increment,
        decrement_line=sp_decrement,
    )
    assert reg.increment_line == sp_increment
    assert reg.decrement_line == sp_decrement


def test_modifyable_address_register_init_exception(high_register, low_register, sp_increment, sp_decrement):
    '''Test if the initisation throws the expected exceptions'''
    low_reg = low_register
    high_reg = high_register
    with pytest.raises(ValueError) as excinfo:
        _ = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg, increment_line=None)
    assert str(excinfo.value) == "No Increment or Decrement Line string was referred to the initialization"
    with pytest.raises(ValueError) as excinfo:
        _ = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg, decrement_line=None)
    assert str(excinfo.value) == "No Increment or Decrement Line string was referred to the initialization"


def test_modifyable_address_register_execute_function(high_register, low_register, address_high_word, address_low_word):
    '''Test if execture_function works as expected'''
    data = 0
    address = 0x000000
    low_reg = low_register
    high_reg = high_register
    low_reg.value = address_low_word
    high_reg.value = address_high_word
    control_signal = {"ZAE": True, "ISP": False, "DSP": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    expected_address = address_low_word | (address_high_word << 16)
    reg = AddressRegister(high_register=high_reg, low_register=low_reg)
    return_bus = reg.execute_function(bus)
    assert return_bus.address == expected_address
    control_signal = {"ZAE": False, "ISP": True, "DSP": False}
    expected_address = (address_low_word | (address_high_word << 16)) + 1
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg)
    return_bus = reg.execute_function(bus)
    control_signal = {"ZAE": True, "ISP": False, "DSP": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    return_bus = reg.execute_function(bus)
    assert return_bus.address == expected_address
    expected_address = address_low_word | (address_high_word << 16)
    control_signal = {"ZAE": False, "ISP": False, "DSP": True}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg)
    return_bus = reg.execute_function(bus)
    control_signal = {"ZAE": True, "ISP": False, "DSP": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    return_bus = reg.execute_function(bus)
    assert return_bus.address == expected_address


def test_modifyable_address_register_execute_function_underoverflow(high_register, low_register):
    '''Test if execute_functions had address under/overflow'''
    data = 0
    address = 0x000000
    low_reg = low_register
    high_reg = high_register
    low_reg.value = 0xFFFF
    high_reg.value = 0xFF

    # Test Overflow
    control_signal = {"ZAE": False, "ISP": True, "DSP": False}
    expected_address = 0x000000
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg)
    return_bus = reg.execute_function(bus)
    control_signal = {"ZAE": True, "ISP": False, "DSP": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    return_bus = reg.execute_function(bus)
    assert return_bus.address == expected_address

    # Test Underflow
    control_signal = {"ZAE": False, "ISP": False, "DSP": True}
    expected_address = 0xFFFFFF
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg)
    return_bus = reg.execute_function(bus)
    control_signal = {"ZAE": True, "ISP": False, "DSP": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    return_bus = reg.execute_function(bus)
    assert return_bus.address == expected_address


def test_modifyable_address_register_execute_function_exceptions(
    high_register, low_register, address_high_word, address_low_word
):
    '''Test if execute_function throws the expected exceptions'''
    data = 0
    low_reg = low_register
    high_reg = high_register
    low_reg.value = address_low_word
    high_reg.value = address_high_word
    address = 0x000000
    control_signal = {"ZAE": False, "ISP": False, "DSP": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg)
    with pytest.raises(ControlBusError) as excinfo:
        _ = reg.execute_function(bus)
    assert str(excinfo.value) == "Unable to write/increment/decrement Address without set control line"
    control_signal = {"ZAE": True, "ISP": True, "DSP": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg)
    with pytest.raises(ControlBusError) as excinfo:
        _ = reg.execute_function(bus)
    assert str(excinfo.value) == "Unable to write Address while icrementing/decrementing"
    control_signal = {"ZAE": True, "ISP": False, "DSP": True}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg)
    with pytest.raises(ControlBusError) as excinfo:
        _ = reg.execute_function(bus)
    assert str(excinfo.value) == "Unable to write Address while icrementing/decrementing"
    control_signal = {"ZAE": False, "ISP": True, "DSP": True}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = ModifiableAddressRegister(high_register=high_reg, low_register=low_reg)
    with pytest.raises(ControlBusError) as excinfo:
        _ = reg.execute_function(bus)
    assert str(excinfo.value) == "Unable to increment and decrement the Address at the same time"
