'''Unit Test for the ALU module'''

from collections import namedtuple
import pytest
import parametrize_from_file
from dwmc_16_emulator.src.modules.alu import Alu
from dwmc_16_emulator.src.modules.exceptions import ControlBusError

# from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import BusData
from dwmc_16_emulator.src.modules.constants import CadrId, AluOp
from dwmc_16_emulator.src.modules.register_bank import RegisterBank

OperationTest = namedtuple(
    'OperationTest',
    ['operation', 'from_data_bus', 'input_a', 'input_b', 'data_bus', 'result', 'initial_flags', 'result_flags'],
)


@pytest.fixture()
def register_bank():
    '''returns a register bank'''
    return RegisterBank()


def test_init_alu(register_bank: RegisterBank):
    '''Testing if initialization works as expected'''
    reg_bank = register_bank
    alu = Alu(reg_bank)
    assert alu.register_bank == reg_bank
    assert alu.flag_register == reg_bank.registers[CadrId.F]


def make_flag_register(flags: list) -> int:
    ret = 0x0000
    for flag in flags:
        ret |= 0x0001 << int(CadrId[flag])
    return ret


# TODO: Add Tests for test operations
@parametrize_from_file
def test_alu_operations(register_bank: RegisterBank, operation, inputs, flags, expected):
    '''Testing ALU operations'''
    reg_bank = register_bank
    alu = Alu(reg_bank)
    alu.flag_register.value = make_flag_register(flags[0])
    addr = 0x000000
    reg_bank.registers[CadrId.R00].value = inputs[0]
    reg_bank.registers[CadrId.R01].value = inputs[1]
    data = inputs[2]
    control_signals = {
        "ALUS": True,
        "AWOE": True,
        "ARDB": flags[1],
        "AREGA": CadrId.R00,
        "AREGB": CadrId.R01,
        "AOPS": AluOp[operation],
        "CADR": inputs[2] & 0x000F,
    }
    bus = BusData(data=data, address=addr, control_signals=control_signals)
    _ = alu.execute_function(bus)
    assert reg_bank.registers[CadrId.R00].value == expected[0]
    assert alu.flag_register.value == make_flag_register(expected[1])


def test_alu_operation_exceptions(register_bank: RegisterBank):
    '''Testing ALU Operation exceptions'''
    reg_bank = register_bank
    alu = Alu(reg_bank)
    alu.flag_register.value = 0x0000
    addr = 0x000000
    reg_bank.registers[CadrId.R00].value = 0x0001
    reg_bank.registers[CadrId.R01].value = 0x0001
    data = 0x0001
    control_signals = {
        "ALUS": False,
        "AWOE": True,
        "ARDB": False,
        "AREGA": CadrId.R00,
        "AREGB": CadrId.R01,
        "AOPS": AluOp.ADD,
        "CADR": 0x1,
    }
    bus = BusData(data=data, address=addr, control_signals=control_signals)
    with pytest.raises(ControlBusError) as excinfo:
        _ = alu.execute_function(bus)
    assert str(excinfo.value) == "ALU not Selected for operation"
    control_signals["ALUS"] = True
    control_signals["AOPS"] = 0x20
    with pytest.raises(ControlBusError) as excinfo:
        _ = alu.execute_function(bus)
    assert str(excinfo.value) == "ALU Operation does not exist"
    control_signals["AOPS"] = AluOp.ADD
    control_signals["AREGA"] = 0x10
    with pytest.raises(ControlBusError) as excinfo:
        _ = alu.execute_function(bus)
    assert str(excinfo.value) == "Selected Register does not exist"
    control_signals["AREGA"] = CadrId.R00
    control_signals["AREGB"] = 0x10
    with pytest.raises(ControlBusError) as excinfo:
        _ = alu.execute_function(bus)
    assert str(excinfo.value) == "Selected Register does not exist"
