'''Unit Test for Memory Module'''

import pytest
from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import BusData
from dwmc_16_emulator.src.modules.memory import Memory


@pytest.fixture()
def address_too_big():
    return 0xFFFFFF + 1


@pytest.fixture()
def address_too_small():
    return 0x000000 - 1


@pytest.fixture()
def address():
    return 0x100000


@pytest.fixture()
def data():
    return 0x1000


def test_init_default():
    '''Testing creation of Memory instance with default parameters'''
    mem = Memory()
    assert len(mem.memory) == 0xFFFFFF


def test_init_size():
    '''Testing creation of Memory instance with different size'''
    mem = Memory(size=0x030000)
    assert mem.size == 0x030000
    assert len(mem.memory) == 0x030000
    del mem
    mem = Memory(rom_size=0x010000)
    assert mem.rom_size == 0x010000


def test_init_memory_exception(address_too_big, address_too_small):
    '''Testing for exceptions in the initialization'''

    # Testing creation of Memory instance with too small and too large sizes
    with pytest.raises(ValueError) as excinfo:
        _ = Memory(size=address_too_big)
    assert str(excinfo.value) == "Memory size is not valid"
    with pytest.raises(ValueError) as excinfo:
        _ = Memory(size=address_too_small)
    assert str(excinfo.value) == "Memory size is not valid"

    # Testing creation of Memory instance with too small or too big ROM area'''
    with pytest.raises(ValueError) as excinfo:
        _ = Memory(rom_size=address_too_big)
    assert str(excinfo.value) == "ROM size out of bounds"
    with pytest.raises(ValueError) as excinfo:
        _ = Memory(rom_size=address_too_small)
    assert str(excinfo.value) == "ROM size out of bounds"


def test_setitem(address, data):
    '''Testing the setter method'''
    mem = Memory()
    mem[address] = data
    assert mem.memory[address] == data


def test_setitem__exception(address_too_big, address_too_small, address, data):
    '''Testing for exception in the setter methos'''

    #  Testing for an exception if the address it outside the memory
    mem = Memory()
    with pytest.raises(ValueError) as excinfo:
        mem[address_too_big] = data
    assert str(excinfo.value) == "Memory address is not valid"
    with pytest.raises(ValueError) as excinfo:
        mem[address_too_small] = data
    assert str(excinfo.value) == "Memory address is not valid"

    # Testing if an exception is raised when the data is more then 16 bits'''
    mem = Memory()
    with pytest.raises(ValueError) as excinfo:
        mem[address] = 0x1FFFF
    assert str(excinfo.value) == "Value too large"


def test_getitem(address, data):
    '''Testing the getter method'''
    mem = Memory()
    mem.memory[address] = data
    assert mem[address] == data


def test_getitem_exception(address_too_big, address_too_small):
    '''Testing for exceptions in the getter method'''

    # Testing for an exception if the address it outside the memory
    mem = Memory()
    with pytest.raises(ValueError) as excinfo:
        _ = mem[address_too_big]
    assert str(excinfo.value) == "Memory address is not valid"
    with pytest.raises(ValueError) as excinfo:
        _ = mem[address_too_small]
    assert str(excinfo.value) == "Memory address is not valid"


def test_execute_function(address, data):
    '''Testing if the execute_function works as expected'''

    # Write
    control_signal = {"WE": True, "IO": False, "RE": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    mem = Memory()
    mem.execute_function(bus)
    assert mem.memory[address] == data

    # Read
    control_signal = {"WE": False, "IO": False, "RE": True}
    address = address + 2
    mem.memory[address] = data
    bus = BusData(data=data, address=address, control_signals=control_signal)
    return_bus = mem.execute_function(bus)
    assert return_bus.data == data

    # Write to ROM
    control_signal = {"WE": True, "IO": False, "RE": False}
    address = 0x001000
    bus = BusData(data=data, address=address, control_signals=control_signal)
    mem = Memory()
    mem.execute_function(bus)
    assert mem.memory[address] != data


def test_execute_function_exceptions(address, data, address_too_big, address_too_small):
    '''Testing for exceptions of the Memory execute_function'''

    # Testing if the IO Exception is raised
    control_signal = {"WE": True, "IO": True, "RE": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    mem = Memory()
    with pytest.raises(ControlBusError) as excinfo:
        mem.execute_function(bus)
    assert str(excinfo.value) == "Unable to access memory if IO Flag is set"

    # Testing if the Read and Write not set exception is raised
    control_signal = {"WE": False, "IO": False, "RE": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    mem = Memory()
    with pytest.raises(ControlBusError) as excinfo:
        mem.execute_function(bus)
    assert str(excinfo.value) == "Unable to access memory if Write and Read Enable is not set"

    # Testing if the Read and Write is set Exception is raised
    control_signal = {"WE": True, "IO": False, "RE": True}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    mem = Memory()
    with pytest.raises(ControlBusError) as excinfo:
        mem.execute_function(bus)
    assert str(excinfo.value) == "Unable to access memory when both Read and Write Enable are set"

    # Testing if the memory addess exceptions are raised
    control_signal = {"WE": True, "IO": False, "RE": False}
    bus = BusData(data=data, address=address_too_big, control_signals=control_signal)
    mem = Memory()
    with pytest.raises(ValueError) as excinfo:
        mem.execute_function(bus)
    assert str(excinfo.value) == "Memory address is not valid"
    control_signal = {"WE": True, "IO": False, "RE": False}
    bus = BusData(data=data, address=address_too_small, control_signals=control_signal)
    with pytest.raises(ValueError) as excinfo:
        mem.execute_function(bus)
    assert str(excinfo.value) == "Memory address is not valid"
