'''Unit Test for Register Modules'''

import pytest
from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import BusData
from dwmc_16_emulator.src.modules.constants import CadrId
