'''Unit Test for Register Modules'''

import pytest
from dwmc_16_emulator.src.modules.exceptions import ControlBusError
from dwmc_16_emulator.src.modules.interfaces import BusData
from dwmc_16_emulator.src.modules.constants import CadrId
from dwmc_16_emulator.src.modules.registers import Register, FlagRegister


@pytest.fixture()
def data():
    return 0x1000


@pytest.fixture()
def address():
    return 0x000000


@pytest.fixture()
def b11():
    return CadrId.B11


@pytest.fixture()
def b12():
    return CadrId.B12


@pytest.fixture()
def ie():
    return CadrId.IE


@pytest.fixture()
def c():
    return CadrId.CA


def test_init_register():
    '''Test the Register Module initialization'''
    reg = Register()
    assert reg.value == 0x0000


def test_init_register_value(data):
    '''Test the Register Module initialization with data'''
    reg = Register(initial_value=data)
    assert reg.value == data


def test_init_register_exceptions():
    '''Test the Register Module exceptions'''
    with pytest.raises(ValueError) as excinfo:
        _ = Register(initial_value=0x10000)
    assert str(excinfo.value) == "Value out of bounds"
    with pytest.raises(ValueError) as excinfo:
        _ = Register(initial_value=(0x0000 - 1))
    assert str(excinfo.value) == "Value out of bounds"


def test_register_execute_function(data, address):
    '''Test execute_function of the Register Module'''

    # Write
    control_signal = {"RWE": True, "RRE": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = Register()
    reg.execute_function(bus)
    assert reg.value == data

    # Read
    del reg
    control_signal = {"RWE": False, "RRE": True}
    reg = Register()
    reg.value = data
    return_bus = reg.execute_function(bus)
    assert return_bus.data == data


def test_register_execute_function_exeptions(data, address):
    '''Test the exceptions of the Register Module execute_function'''

    # Is Exception raised if Read and Write are not set?
    control_signal = {"RWE": False, "RRE": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = Register()
    with pytest.raises(ControlBusError) as excinfo:
        reg.execute_function(bus)
    assert str(excinfo.value) == "Unable to access register if Register Read and Write Enable is not set"

    # Is Exception raised if Read and Write are set?
    control_signal = {"RWE": True, "RRE": True}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = Register()
    with pytest.raises(ControlBusError) as excinfo:
        reg.execute_function(bus)
    assert str(excinfo.value) == "Unable to access register if Register Read and Write Enable are set"

    # Is Exception raised if value is too large (17 bit)?
    control_signal = {"RWE": True, "RRE": False}
    data = 0x10000
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = Register()
    with pytest.raises(ValueError) as excinfo:
        reg.execute_function(bus)
    assert str(excinfo.value) == "Value too large"


def test_flagregister_execute_function(data, address, b11, b12):
    '''Test if TestRegister execute Function works as expected'''

    # Bit 11 is not set
    reg = FlagRegister()
    reg.value = data
    control_signal = {"RWE": False, "RRE": False, "CADR": b11, "FT": True, "BFS": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    return_bus = reg.execute_function(bus)
    assert return_bus.control_signals["BFS"] == False

    # Bit 12 is set
    control_signal["CADR"] = b12
    bus = BusData(data=data, address=address, control_signals=control_signal)
    return_bus = reg.execute_function(bus)
    assert return_bus.control_signals["BFS"] == True


def test_flagregister_execute_function_exeptions(data, address):
    '''Test the exceptions of the Register Module execute_function'''

    # Is Exception raised if Read, Write and Bit Test are not set?
    control_signal = {"RWE": False, "RRE": False, "CADR": b11, "FT": False, "BFS": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = FlagRegister()
    with pytest.raises(ControlBusError) as excinfo:
        reg.execute_function(bus)
    assert str(excinfo.value) == "Unable to access register if Register Read and Write Enable and Bit Test are not set"

    # Is Exception raised if Read, Write and Bit Test are set?
    control_signal = {"RWE": True, "RRE": True, "CADR": b11, "FT": True, "BFS": False}
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = FlagRegister()
    with pytest.raises(ControlBusError) as excinfo:
        reg.execute_function(bus)
    assert str(excinfo.value) == "Unable to access register if Register Read and Write Enable and Bit Test are set"

    # Is Exception raised if value is too large (17 bit)?
    control_signal = {"RWE": True, "RRE": False}
    data = 0x10000
    bus = BusData(data=data, address=address, control_signals=control_signal)
    reg = FlagRegister()
    with pytest.raises(ValueError) as excinfo:
        reg.execute_function(bus)
    assert str(excinfo.value) == "Value too large"


def test_flagregister_set_flag(ie, data):
    '''Test if flag in register is set ny set_flag'''
    reg = FlagRegister()
    reg.set_flag(ie)
    assert reg.value == data


def test_flagregister_reset_flag(ie, data):
    '''Test if flag is reset in register by reset_flag'''
    reg = FlagRegister()
    reg.value = data
    reg.reset_flag(ie)
    assert reg.value == 0x0000
